<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   24-May-19
 */

namespace alexs\yii2crud\tests;
use alexs\yii2crud\tests\models\ArticleSorted;
use alexs\yii2phpunittestcase\DatabaseTableTestCase;

class CrudModelSortedTest extends DatabaseTableTestCase
{
    public function testSavingGetSortMaxValue() {
        $ArticleSorted = new ArticleSorted;
        $this->assertSame($ArticleSorted->getSortMaxValue(), 0);
        for ($i = 1; $i <= 3; $i ++) {
            $data = [
                'id'   =>$i,
                'title'=>'Article ' . $i,
                'text' =>'Article contents' . $i,
            ];
            $Article = new ArticleSorted;
            $Article->setAttributes($data);
            $Article->save();
        }
        for ($i = 1; $i <= 3; $i ++) {
            $Article = ArticleSorted::findOne($i);
            $this->assertSame($i, $Article->pos);
        }
        $this->assertSame($ArticleSorted->getSortMaxValue(), 3);
    }

    protected function getTableName() {
        return 'article_sorted';
    }

    protected function getTableColumns() {
        return [
            'id'   =>'pk',
            'title'=>'string NOT NULL',
            'text' =>'string NOT NULL',
            'pos'  =>'integer DEFAULT 0',
        ];
    }
}
