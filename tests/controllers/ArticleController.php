<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   06.09.2017
 */

namespace alexs\yii2crud\tests\controllers;
use alexs\yii2crud\controllers\CrudController;

class ArticleController extends CrudController
{
    public $actions_namespace = '\alexs\yii2crud\tests\actions';

    public $model_namespace = '\alexs\yii2crud\tests\models';

    public function actions() {
        return array_merge(parent::actions(), [
            'index-without-pagination'=>'alexs\yii2crud\tests\actions\IndexWithoutPaginationAction',
        ]);
    }
}