<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   13-Sep-17
 */

namespace alexs\yii2crud\tests;
use alexs\yii2phpunittestcase\DatabaseTableTestCase;
use alexs\yii2crud\controllers\CrudController;
use Yii;

class CrudControllerTest extends DatabaseTableTestCase
{
    /** @var CrudController $controller */
    protected $controller;

    public function testGetModelClassName() {
        $this->assertSame('Article', $this->controller->getModelClassName());
    }

    public function testGetModelName() {
        $this->assertSame('\alexs\yii2crud\tests\models\Article', $this->controller->getModelName());
    }

    public function testSetActionAliasUrl() {
        $this->controller->setActionAliasUrl('doit');
        $this->assertSame('article/doit', Yii::getAlias('@action_doit'));
    }

    protected function setUp() {
        parent::setUp();
        Yii::$app->controllerNamespace = 'alexs\\yii2crud\\tests\\controllers';
        Yii::$app->runAction('/article/index');
        $this->controller = Yii::$app->controller;
    }

    protected function getTableName() {
        return 'article';
    }

    protected function getTableColumns() {
        return [
            'id'   =>'pk',
            'title'=>'string NOT NULL',
            'text' =>'string NOT NULL',
        ];
    }
}
