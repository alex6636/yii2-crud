<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   13-Sep-17
 */

namespace alexs\yii2crud\tests;
use alexs\yii2crud\tests\models\Article;
use alexs\yii2phpunittestcase\DatabaseTableTestCase;
use Yii;

class CrudActionsTest extends DatabaseTableTestCase
{
    public function testIndexAction() {
        for ($i = 1; $i <= 3; $i ++) {
            $data = [
                'id'   =>$i,
                'title'=>'Article ' . $i,
                'text' =>'Article contents' . $i,
            ];
            $Article = new Article;
            $Article->setAttributes($data);
            $Article->save();
        }
        $result = Yii::$app->runAction('/article/index');
        $this->assertInstanceOf('alexs\yii2crud\models\CrudModel', $result['models'][0]);
        $this->assertInstanceOf('yii\data\Pagination', $result['Pagination']);
    }

    public function testIndexWithoutPaginationAction() {
        for ($i = 1; $i <= 3; $i ++) {
            $data = [
                'id'   =>$i,
                'title'=>'Article ' . $i,
                'text' =>'Article contents' . $i,
            ];
            $Article = new Article;
            $Article->setAttributes($data);
            $Article->save();
        }
        $result = Yii::$app->runAction('/article/index-without-pagination');
        $this->assertInstanceOf('alexs\yii2crud\models\CrudModel', $result['models'][0]);
        $this->assertNull($result['Pagination']);
    }

    public function testAddAction() {
        $data = [
            'id'   =>1,
            'title'=>'First article',
            'text' =>'First article contents',
        ];
        Yii::$app->request->setBodyParams(['Article'=>$data]);
        Yii::$app->runAction('/article/add');
        $this->assertEquals($data, Article::find()->where(['id'=>1])->asArray(true)->one());
    }

    public function testAddActionInvalidData() {
        $request_data = [
            'id'   =>'',
            'title'=>'',
            'text' =>'First article contents',
        ];
        Yii::$app->request->setBodyParams(['Article'=>$request_data]);
        Yii::$app->runAction('/article/add');
        $this->assertNull(Article::find()->where(['id'=>1])->asArray(true)->one());
    }
    
    public function testEditAction() {
        $data = [
            'id'   =>1,
            'title'=>'First article',
            'text' =>'First article contents',
        ];
        $Article = new Article;
        $Article->setAttributes($data);
        $Article->save();
        $data['title'] = 'First article edited';
        Yii::$app->request->setBodyParams(['Article'=>$data]);
        Yii::$app->runAction('/article/edit', ['id'=>$data['id']]);
        $this->assertEquals($data, Article::find()->where(['id'=>1])->asArray(true)->one());
    }
    
    public function testEditActionInvalidData() {
        $data = [
            'id'   =>1,
            'title'=>'First article',
            'text' =>'First article contents',
        ];
        $Article = new Article;
        $Article->setAttributes($data);
        $Article->save();
        $data_after_insert = $data;
        $data['title'] = '';
        Yii::$app->request->setBodyParams(['Article'=>$data]);
        Yii::$app->runAction('/article/edit', ['id'=>$data['id']]);
        $this->assertEquals($data_after_insert, Article::find()->where(['id'=>1])->asArray(true)->one());
    }

    /**
     * @expectedException \yii\web\NotFoundHttpException
     */
    public function testEditActionNotFound() {
        Yii::$app->runAction('/article/edit', ['id'=>123]);       
    }

    public function testDeleteAction() {
        $data = [
            'id'   =>1,
            'title'=>'First article',
            'text' =>'First article contents',
        ];
        $Article = new Article;
        $Article->setAttributes($data);
        $Article->save();
        Yii::$app->runAction('/article/delete', ['id'=>$data['id']]);
        $this->assertEquals(NULL, Article::find()->where(['id'=>1])->asArray(true)->one());
    }
    
    /**
     * @expectedException \yii\web\NotFoundHttpException
     */
    public function testDeleteActionNotFound() {
        Yii::$app->runAction('/article/delete', ['id'=>123]);       
    }
    
    protected function setUp() {
        parent::setUp();
        Yii::$app->controllerNamespace = 'alexs\\yii2crud\\tests\\controllers';
    }

    protected function getTableName() {
        return 'article';
    }

    protected function getTableColumns() {
        return [
            'id'   =>'pk',
            'title'=>'string NOT NULL',
            'text' =>'string NOT NULL',
        ];
    }
}
