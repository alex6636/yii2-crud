<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   06.09.2017
 */

namespace alexs\yii2crud\tests\models;
use alexs\yii2crud\models\CrudModel;

/**
 * @property int $id
 * @property string $title
 * @property string $text
 */

class Article extends CrudModel
{
    public function rules() {
        return [
            [['title', 'text'], 'filter', 'filter'=>'trim'],
            [['id', 'title'], 'required'],
            ['id', 'integer'],
        ];
    }
}
