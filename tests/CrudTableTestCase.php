<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   13-Sep-17
 */

namespace alexs\yii2crud\tests;
use alexs\yii2phpunittestcase\DatabaseTableTestCase;

abstract class CrudTableTestCase extends DatabaseTableTestCase
{
    protected function getTableName() {
        return 'article';
    }

    protected function getTableColumns() {
        return [
            'id'   =>'pk',
            'title'=>'string NOT NULL',
            'text' =>'string NOT NULL',
            'pos'  =>'integer DEFAULT 0',
        ];
    }
}
