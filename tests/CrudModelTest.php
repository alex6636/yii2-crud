<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   13-Sep-17
 */

namespace alexs\yii2crud\tests;
use alexs\yii2crud\tests\models\Article;
use alexs\yii2crud\tests\models\ArticleSorted;
use alexs\yii2phpunittestcase\DatabaseTableTestCase;
use Yii;

class CrudModelTest extends DatabaseTableTestCase
{
    public function testFindItems() {
        $this->assertInstanceOf('yii\db\ActiveQuery', Article::findItems());
    }

    public function testListAll() {
        for ($i = 1; $i <= 3; $i ++) {
            $data = [
                'id' => $i,
                'title' => 'Article ' . $i,
                'text' => 'Article contents ' . $i,
            ];
            $Article = new Article;
            $Article->setAttributes($data);
            $Article->save();
        }
        $this->assertEquals([
            '1'=>'Article 1',
            '2'=>'Article 2',
            '3'=>'Article 3',
        ], Article::listAll('title'));

        $this->assertEquals([
            'Article 1'=>'Article contents 1',
            'Article 2'=>'Article contents 2',
            'Article 3'=>'Article contents 3',
        ], Article::listAll('text', 'title'));
    }

    public function testGetModelName() {
        $this->assertEquals('Article', Article::getModelName());
    }

    public function testGetUploadDirName() {
        $this->assertEquals('article', Article::getUploadDirName());
        $this->assertEquals('article-sorted', ArticleSorted::getUploadDirName());
    }

    public function testGetUploadDir() {
        Yii::setAlias('@uploads_dir', 'C:/uploads');
        $this->assertEquals('C:/uploads/article', Article::getUploadDir());
        $this->assertEquals('C:/uploads/article-sorted', ArticleSorted::getUploadDir());
        Yii::setAlias('@uploads_dir', 'C:/uploads/');
        $this->assertEquals('C:/uploads/article', Article::getUploadDir());
        $this->assertEquals('C:/uploads/article-sorted', ArticleSorted::getUploadDir());
    }

    public function testUploadUrl() {
        Yii::$app->request->baseUrl = '/';
        Yii::setAlias('@uploads_url', '/uploads');
        $this->assertEquals('/uploads/article', Article::getUploadURL());
        $this->assertEquals('/uploads/article-sorted', ArticleSorted::getUploadURL());
        Yii::setAlias('@uploads_url', '/uploads/');
        $this->assertEquals('/uploads/article', Article::getUploadURL());
        $this->assertEquals('/uploads/article-sorted', ArticleSorted::getUploadURL());
    }

    public function testGetUploadedFilePath() {
        Yii::setAlias('@uploads_dir', 'C:/uploads');
        $data = [
            'id' => 1,
            'title' => 'Article 1',
            'text' => 'Article contents 1',
        ];
        $Article = new Article;
        $Article->setAttributes($data);
        $Article->save();
        $this->expectException('InvalidArgumentException');
        $Article->getUploadedFilePath('image');
        $this->assertNull($Article->getUploadedFilePath('file'));
        $Article->file = 'file.txt';
        $this->assertEquals('C:/uploads/article/file.txt', $Article->getUploadedFilePath('file'));
        $this->assertEquals('C:/uploads/article/tmp/file.txt', $Article->getUploadedFilePath('file', 'tmp'));
        $this->assertEquals('C:/uploads/article/tmp/file.txt', $Article->getUploadedFilePath('file', '/tmp/'));
    }

    public function testGetUploadedFileUrl() {
        Yii::setAlias('@uploads_url', '/uploads');
        $data = [
            'id' => 1,
            'title' => 'Article 1',
            'text' => 'Article contents 1',
        ];
        $Article = new Article;
        $Article->setAttributes($data);
        $Article->save();
        $this->expectException('InvalidArgumentException');
        $Article->getUploadedFilePath('image');
        $this->assertNull($Article->getUploadedFileURL('file'));
        Yii::$app->request->baseUrl = '/';
        $Article->file = 'file.txt';
        $this->assertEquals('/uploads/article/file.txt', $Article->getUploadedFileURL('file'));
        $this->assertEquals('/uploads/article/tmp/file.txt', $Article->getUploadedFileURL('file', 'tmp'));
        $this->assertEquals('/uploads/article/tmp/file.txt', $Article->getUploadedFileURL('file', '/tmp/'));
    }

    protected function getTableName() {
        return 'article';
    }

    protected function getTableColumns() {
        return [
            'id'   =>'pk',
            'title'=>'string NOT NULL',
            'text' =>'string NOT NULL',
            'file' =>'string DEFAULT NULL',
        ];
    }
}
