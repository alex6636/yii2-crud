<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   14-Sep-17
 */
namespace alexs\yii2crud\tests\actions;

class IndexWithoutPaginationAction extends IndexAction
{
    public $pagination = false;

    /**
     * @param string $layout
     * @param string $view
     * @param array $params
     * @return mixed
     */
    protected function displayView($layout, $view, $params) {
        return $params;
    }
}