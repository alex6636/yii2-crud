<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   07-Sep-17
 */

namespace alexs\yii2crud\tests\actions;
use alexs\yii2crud\models\CrudModel;
use yii\web\Response;

class AddAction extends \alexs\yii2crud\actions\AddAction
{
    /**
     * @param string $layout
     * @param string $view
     * @param array $params
     * @return mixed
     */
    protected function displayView($layout, $view, $params) {
        return null;
    }

    /**
     * @param CrudModel $Model
     * @return Response|NULL
     */
    protected function afterAdd(CrudModel $Model) {
        return null;
    }
}
