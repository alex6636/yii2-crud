<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   07-Sep-17
 */

namespace alexs\yii2crud\tests\actions;
use alexs\yii2crud\models\CrudModel;
use yii\web\Response;

class DeleteAction extends \alexs\yii2crud\actions\DeleteAction
{
    /**
     * @param CrudModel $Model
     * @return Response|NULL
     */
    protected function afterDelete(CrudModel $Model) {
        return null;
    }
}
