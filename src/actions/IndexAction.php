<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   24-May-19
 */

namespace alexs\yii2crud\actions;
use alexs\yii2crud\controllers\CrudController;
use alexs\yii2crud\models\CrudModel;
use yii\data\Pagination;
use yii\db\ActiveQuery;

class IndexAction extends CrudAction
{
    public $layout = 'index';
    public $view   = 'index';
    public $pagination = true;
    public $pagination_pagesize = 20;

    /**
     * @return string|null
     */
    public function run() {
        /** @var CrudController $controller */
        $controller = $this->controller;
        /** @var CrudModel $Model */
        $Model = $controller->getModelName();
        $ActiveQuery = $Model::findItems();
        $Pagination = $this->paginate($ActiveQuery);
        $models = $ActiveQuery->all();
        return $this->displayView($this->layout, $this->view, [
            'models'=>$models,
            'Pagination'=>$Pagination,
        ]);
    }

    /**
     * @param ActiveQuery $ActiveQuery
     * @return Pagination|null
     */
    protected function paginate(ActiveQuery $ActiveQuery) {
        if (!$this->pagination) {
            return null;
        }
        $Pagination = new Pagination([
            'totalCount'=>$ActiveQuery->count(),
            'pageSize'  =>$this->pagination_pagesize,
        ]);
        $ActiveQuery->offset($Pagination->offset)->limit($Pagination->limit);
        return $Pagination;
    }
}
