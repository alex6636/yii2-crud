<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   07-Sep-17
 */

namespace alexs\yii2crud\actions;
use alexs\yii2crud\controllers\CrudController;
use yii\base\Action;

abstract class CrudAction extends Action
{
    /**
     * @param string $layout
     * @param string $view
     * @param array $params
     * @return mixed
     */
    protected function displayView($layout, $view, $params) {
        /** @var CrudController $controller */
        $controller = $this->controller;
        $controller->layout = $layout;
        return $controller->render($view, $params);
    }
}