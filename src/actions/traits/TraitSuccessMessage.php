<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   26.05.19
 */

namespace alexs\yii2crud\actions\traits;
use alexs\yii2crud\models\CrudModel;

trait TraitSuccessMessage
{
    use TraitMessageable;

    protected $success_message_key = 'message';
    
    /**
     * @param CrudModel $Model
     * @return string
     */
    abstract protected function getSuccessMessageText(CrudModel $Model);

    /**
     * @param CrudModel $Model
     */
    protected function displaySuccessMessage($Model) {
        $this->displayMessage($this->getSuccessMessageText($Model), $this->success_message_key);
    }
}
