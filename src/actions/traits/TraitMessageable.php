<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   26.05.19
 */

namespace alexs\yii2crud\actions\traits;

trait TraitMessageable
{
    /**
     * Display the message
     * 
     * @param string $message
     * @param string $key
     */
    protected function displayMessage($message, $key = 'message') {
        \Yii::$app->session->setFlash($key, $message);
    }
}
