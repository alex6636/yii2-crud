<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   15-Aug-18
 */

namespace alexs\yii2crud\actions;
use alexs\yii2crud\models\CrudModel;

class EditOnlyAction extends EditAction
{
    /**
     * @param CrudModel $Model
     * @return string
     */
    protected function afterEdit(CrudModel $Model) {
        $this->displaySuccessMessage($Model);
        return $this->displayView($this->layout, $this->view, [
            'Model'=>$Model,
        ]);
    }
}
