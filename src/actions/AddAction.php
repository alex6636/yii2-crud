<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   07-Sep-17
 */

namespace alexs\yii2crud\actions;
use alexs\yii2crud\actions\traits\TraitSuccessMessage;
use alexs\yii2crud\controllers\CrudController;
use alexs\yii2crud\models\CrudModel;
use yii\web\Response;

class AddAction extends CrudAction
{
    use TraitSuccessMessage;
    
    public $scenario = CrudModel::SCENARIO_DEFAULT;
    public $layout = 'form';
    public $view   = 'form';

    /**
     * @return string|null
     */
    public function run() {
        /** @var CrudController $controller */
        $controller = $this->controller;
        /** @var CrudModel|string $model_name */
        $model_name = $controller->getModelName();
        /** @var CrudModel $Model */
        $Model = new $model_name;
        $Model->setScenario($this->scenario);
        if ($Model->load($this->getData()) && $Model->validate()) {
            $Model->save(false);
            return $this->afterAdd($Model);
        }
        return $this->displayView($this->layout, $this->view, [
            'Model'=>$Model,
        ]);
    }

    /**
     * @param CrudModel $Model
     * @return string
     */
    protected function getSuccessMessageText($Model) {
        return \Yii::t('app', $Model::getHumanModelName() . ' has been successfully added');
    }
    
    /**
     * @return array|mixed
     */
    protected function getData() {
        return \Yii::$app->request->post();
    }

    /**
     * @param CrudModel $Model
     * @return Response|string
     */
    protected function afterAdd(CrudModel $Model) {
        /** @var CrudController $controller */
        $controller = $this->controller;
        $this->displaySuccessMessage($Model);
        return $controller->redirect($controller->getRedirectUrl('@action_index'));
    }
}
