<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   26.05.2019
 */

namespace alexs\yii2crud\actions;
use alexs\yii2crud\actions\traits\TraitSuccessMessage;
use alexs\yii2crud\controllers\CrudController;
use alexs\yii2crud\models\CrudModel;
use yii\web\Response;
use yii\web\NotFoundHttpException;

class DeleteAction extends CrudAction
{
    use TraitSuccessMessage;
    
    /**
     * @param int $id
     * @return string|null
     * @throws NotFoundHttpException
     */
    public function run($id) {
        /** @var CrudModel $Model */
        if (!$Model = static::findModel($id)) {
            throw new NotFoundHttpException;
        }
        $Model->delete();
        return $this->afterDelete($Model);
    }

    /**
     * @param CrudModel $Model
     * @return string
     */
    protected function getSuccessMessageText($Model) {
        return \Yii::t('app', $Model::getHumanModelName() . ' has been successfully deleted');
    }
    
    /**
     * @param int $id
     * @return CrudModel|null
     */
    protected function findModel($id) {
        /** @var CrudController $controller */
        $controller = $this->controller;
        /** @var CrudModel $model_name */
        $model_name = $controller->getModelName();
        return $model_name::findOne($id);
    }
    
    /**
     * @param CrudModel $Model
     * @return Response|null
     */
    protected function afterDelete(CrudModel $Model) {
        /** @var CrudController $controller */
        $controller = $this->controller;
        $this->displaySuccessMessage($Model);
        return $controller->redirect($controller->getRedirectUrl('@action_index'));
    }
}
