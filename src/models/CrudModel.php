<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   06.09.2017
 */

namespace alexs\yii2crud\models;
use yii\db\ActiveRecord;
use yii\db\ActiveQuery;
use yii\helpers\StringHelper;
use yii\helpers\ArrayHelper;
use yii\helpers\Inflector;
use yii\helpers\Url;

/**
 * @property int $id
 */

abstract class CrudModel extends ActiveRecord
{
    /**
     * Return the list of items
     *
     * @return ActiveQuery
     */
    public static function findItems() {
        return static::find();
    }

    /**
     * Return the list of items as an array
     *
     * @param string $value_attr
     * @param string|null $key_attr is primary key by default
     * @return array
     */
    public static function listAll($value_attr, $key_attr = null) {
        $key_attr = ($key_attr !== null) ? $key_attr : static::primaryKey()[0];
        $ActiveQuery = static::listAllQuery($value_attr, $key_attr)->asArray();
        return ArrayHelper::map($ActiveQuery->all(), $key_attr, $value_attr);
    }

    /**
     * Return the query for list of items
     *
     * @param string $value_attr
     * @param string|null $key_attr
     * @return ActiveQuery
     */
    public static function listAllQuery($key_attr, $value_attr) {
        return static::find()->select([$key_attr, $value_attr]);
    }

    /**
     * @return string
     */
    public static function getModelName() {
        return StringHelper::basename(static::class);
    }

    /**
     * The name for use on HTML-pages, messages, etc.
     * Make sense to redeclare for models with complex names
     * 
     * @return string
     */
    public static function getHumanModelName() {
        return static::getModelName();
    }
    
    /**
     * CamelCaseModelName to camel-case-model-name
     *
     * @return string
     */
    public static function getUploadDirName() {
        return Inflector::camel2id(static::getModelName());
    }

    /**
     * @return string
     */
    public static function getUploadDir() {
        return rtrim(\Yii::getAlias('@uploads_dir'), '/') . '/' . static::getUploadDirName();
    }

    /**
     * @param bool $scheme
     * @return string
     */
    public static function getUploadURL($scheme = false) {
        return ltrim(Url::base($scheme), '/') . rtrim(\Yii::getAlias('@uploads_url'), '/') . '/' . static::getUploadDirName();
    }

    /**
     * @param string $attribute
     * @param string|null $subdir
     * @return string|null
     */
    public function getUploadedFilePath($attribute, $subdir = null) {
        if (!$this->hasAttribute($attribute)) {
            throw new \InvalidArgumentException("Attribute $attribute is invalid");
        }
        if (empty($this->$attribute)) {
            return null;
        }
        $subdir = $subdir ? ltrim(rtrim($subdir, '/'), '/') . '/' : '';
        return static::getUploadDir() . '/' . $subdir . $this->$attribute;
    }

    /**
     * @param string $attribute
     * @param string|null $subdir
     * @param bool $scheme
     * @return string|null
     */
    public function getUploadedFileURL($attribute, $subdir = null, $scheme = false) {
        if (!$this->hasAttribute($attribute)) {
            throw new \InvalidArgumentException("Attribute $attribute is invalid");
        }
        if (empty($this->$attribute)) {
            return null;
        }
        $subdir = $subdir ? ltrim(rtrim($subdir, '/'), '/') . '/' : '';
        return static::getUploadURL($scheme) . '/' . $subdir . $this->$attribute;
    }
}
