<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   24-May-19
 */

namespace alexs\yii2crud\models;
use yii\db\ActiveQuery;

/**
 * @property int $id
 * @property int $pos
 */

abstract class CrudModelSorted extends CrudModel
{
    public static $sort_attribute = 'pos';

    /**
     * Return items list
     *
     * @return ActiveQuery
     */
    public static function findItems() {
        return static::find()->orderBy(static::$sort_attribute);
    }

    /**
     * Return query for items list
     *
     * @param string $value_attr
     * @param string|null $key_attr
     * @return ActiveQuery
     */
    public static function listAllQuery($key_attr, $value_attr) {
        return static::find()->select([$key_attr, $value_attr])->orderBy(static::$sort_attribute);
    }

    /**
     * @return int
     */
    public function getSortMaxValue() {
        return (int) static::find()->select('MAX(' . static::$sort_attribute . ')')->scalar();
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert) {
        if ($insert) {
            $this->{static::$sort_attribute} = $this->getSortMaxValue() + 1;
        }
        return parent::beforeSave($insert);
    }
}
