<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   06.09.2017
 */

namespace alexs\yii2crud\controllers;
use yii\web\Controller;
use yii\helpers\StringHelper;
use yii\helpers\Url;

abstract class CrudController extends Controller
{
    public $actions_namespace = '\alexs\yii2crud\actions';

    public $model_namespace = '\app\models';

    public function beforeAction($action) {
        $this->setActionAliasUrl(['index', 'add', 'edit', 'delete']);
        return parent::beforeAction($action);
    }

    public function actions() {
        $actions_namespace = rtrim($this->actions_namespace, '\\');
        return array_merge(parent::actions(), [
            'index' =>$actions_namespace . '\IndexAction',
            'add'   =>$actions_namespace . '\AddAction',
            'edit'  =>$actions_namespace . '\EditAction',
            'delete'=>$actions_namespace . '\DeleteAction',
        ]);
    }

    /**
     * @return string
     */
    public function getModelClassName() {
        return str_replace('Controller', '', StringHelper::basename(static::class));
    }

    /**
     * @return string
     */
    public function getModelName() {
        return rtrim($this->model_namespace, '\\') . '\\' . $this->getModelClassName();
    }

    /**
     * @param string|array $action
     * for example 'add', 'edit' or ['add', 'edit']
     * @return $this
     */
    public function setActionAliasUrl($action) {
        $action = (array) $action;
        foreach ($action as $action_name) {
            \Yii::setAlias('@action_' . $action_name,  $this->id . '/' . $action_name);
        }
        return $this;
    }

    /**
     * @return array
     */
    public function getRedirectUrlParams() {
        return [];
    }

    /**
     * For example Yii::$app->controller->getRedirectUrl('@action_edit', ['id'=>5])
     * @param string $alias
     * @param array $params
     * @return string
     */
    public function getRedirectUrl($alias, array $params = []) {
        return Url::to(array_merge([\Yii::getAlias($alias)], $this->getRedirectUrlParams(), $params));
    }
}
